//var epilog=angular.module('epilog', ['ionic', 'starter.services']);

epilog.directive('fileModel', function ($parse) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind('change', function () {
                scope.$apply(function () {
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
});

 epilog.service('fileUpload', ['$http', function ($http) {
    this.uploadFileToUrl = function(file, uploadUrl){
       var fd = new FormData();
       fd.append('file', file);
    
       $http.post(uploadUrl, fd, {
          transformRequest: angular.identity,
          headers: {'Content-Type': undefined}
       })
    
       .success(function(){
       })
    
       .error(function(){
       });
    }
 }]);
 

epilog.factory('GEOMap', function () {
    var coord = new Array();
    var title;
    var info;

    return {

        showGeoMap: function (c, t, i) {
            coord = c;
            title = t;
            info = i;
        },
        geoMap: function (mapElement, $scope) {

            var myLatlng;
            var map;
            var marker;

            myLatlng = new google.maps.LatLng(coord[0], coord[1]);

            var mapOptions = {
                zoom: 17,
                center: myLatlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                scrollwheel: false,
                draggable: false//,


                // Disables the default Google Maps UI components
               // disableDefaultUI: true,


               /* styles: [{
                    "featureType": "water",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#000000"
            }, {
                        "lightness": 17
            }]
        }, {
                    "featureType": "landscape",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#000000"
            }, {
                        "lightness": 20
            }]
        }, {
                    "featureType": "road.highway",
                    "elementType": "geometry.fill",
                    "stylers": [{
                        "color": "#000000"
            }, {
                        "lightness": 17
            }]
        }, {
                    "featureType": "road.highway",
                    "elementType": "geometry.stroke",
                    "stylers": [{
                        "color": "#000000"
            }, {
                        "lightness": 29
            }, {
                        "weight": 0.2
            }]
        }, {
                    "featureType": "road.arterial",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#000000"
            }, {
                        "lightness": 18
            }]
        }, {
                    "featureType": "road.local",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#000000"
            }, {
                        "lightness": 16
            }]
        }, {
                    "featureType": "poi",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#000000"
            }, {
                        "lightness": 21
            }]
        }, {
                    "elementType": "labels.text.stroke",
                    "stylers": [{
                        "visibility": "on"
            }, {
                        "color": "#000000"
            }, {
                        "lightness": 16
            }]
        }, {
                    "elementType": "labels.text.fill",
                    "stylers": [{
                        "saturation": 36
            }, {
                        "color": "#000000"
            }, {
                        "lightness": 40
            }]
        }, {
                    "elementType": "labels.icon",
                    "stylers": [{
                        "visibility": "off"
            }]
        }, {
                    "featureType": "transit",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#000000"
            }, {
                        "lightness": 19
            }]
        }, {
                    "featureType": "administrative",
                    "elementType": "geometry.fill",
                    "stylers": [{
                        "color": "#000000"
            }, {
                        "lightness": 20
            }]
        }, {
                    "featureType": "administrative",
                    "elementType": "geometry.stroke",
                    "stylers": [{
                        "color": "#000000"
            }, {
                        "lightness": 17
            }, {
                        "weight": 1.2
            }]
        }]*/
        };
        if (mapElement != null) {
            map = new google.maps.Map(mapElement, mapOptions);
        }

        var contentString = "<div><a ng-click='clickTest()'>" + title + "</a></div>" + '<p style="line-height: 20px;"><strong>' + title + '</strong></p><p>' + info + '</p>';

        var infowindow = new google.maps.InfoWindow({
            content: contentString
        });

        this.addMarker(myLatlng, map);
        google.maps.event.addListener(marker, 'click', function () {
            infowindow.open(map, marker);
        });
        $scope.map = map;

    },
    addMarker:function(myLatlng, map){
        marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
            title: 'Marker'
        });
    }
}
});