

epilog.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  // .state('app.search', {
  //   url: '/search',
  //   views: {
  //     'menuContent': {
  //       templateUrl: 'templates/search.html'
  //     }
  //   }
  // })
    .state('app.logs', {
      url: '/logs',
      views: {
        'menuContent': {
          templateUrl: 'templates/logs/list.html',
          controller: 'LogListCtrl'
        }
      }
    })
    .state('app.addSymptoms', {
      url: '/createlog',
      views: {
        'menuContent': {
          templateUrl: 'templates/logs/create.html',
          controller: 'LogCreateCtrl'
        }
      }
    })

  .state('app.map', {
    url: '/map',
    views: {
      'menuContent': {
        templateUrl: 'templates/map.html',
        controller: 'MapCtrl'
      }
    }
  });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/logs');
});
