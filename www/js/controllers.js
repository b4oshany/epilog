//var epilog=angular.module('starter.controllers', []);


epilog.controller('AppCtrl', function ($scope, $http, $ionicModal, $timeout, SERVER) {

    // With the new view caching in Ionic, Controllers are only called
    // when they are recreated or on app start, instead of every page change.
    // To listen for when this page is active (for example, to refresh data),
    // listen for the $ionicView.enter event:
    //$scope.$on('$ionicView.enter', function(e) {
    //});

    // Form data for the login modal
    $scope.loginData = {};

    // Create the login modal that we will use later
    $ionicModal.fromTemplateUrl('templates/login.html', {
        scope: $scope
    }).then(function (modal) {
        $scope.modal = modal;
    });

    // Triggered in the login modal to close it
    $scope.closeLogin = function () {
        $scope.modal.hide();
    };

    // Open the login modal
    $scope.login = function () {
        $scope.modal.show();
    };

    // Perform the login action when the user submits the login form
    $scope.doLogin = function () {
        console.log('Doing login', $scope.loginData);
        var data = {
            "username": $scope.loginData.username,
            "password": $scope.loginData.password
        };
        $http.post(SERVER+'/api/login', data).then(function (){
            console.log('User was login successfully');
            localStorage.setItem("userdata", JSON.stringify(data))
            $scope.closeLogin();
        }, function (){
            console.log("'User wasn't login successfully'");
        });
    };
});



epilog.controller('LogListCtrl', function ($scope, $rootScope, $sce, $state, $stateParams, SERVER, $http) {
    $scope.pageID = "symptoms";
    $rootScope.pageID = "symptoms";
    $scope.pageTitle = "My Symptoms";
    $scope.logs = [];
    $scope.profile = {"status": "You're just sick", "status_id": "sick"};
    var username = $scope.getUserID();

    var updateRecoveryStatus = function(recovery_status){
        if(recovery_status == 100){
            $scope.profile["status"] =  $sce.trustAsHtml("Fully <br/> Recovered");
            $scope.profile["status_id"] = "recovered";
        }if(recovery_status >= 25){
            $scope.profile["status"] = $sce.trustAsHtml("Getting \n Better");
            $scope.profile["status_id"] = "better";
        }else{
            $scope.profile["status"] = "You're <br/> just sick";
            $scope.profile["status_id"] = "sick";                
        }
    };
    
    $http.get(SERVER+"/api/logs/"+username+"/").then(
        function(response){
            $scope.logs = response.data.logs;
            var recovery_status = response.data.logs.recovery_status;
            updateRecoveryStatus(updateRecoveryStatus);
        },
        function(response){
            console.log("Error occured")
        }
    );

    $scope.updateSymptom = function(event, log_id, symptom_id){
        var element = document.querySelector("input[name='symptom-id"+symptom_id+"']");
        console.log(element);
        console.log(event.target);
//        var data = {"status": 0};
//        if(elem.getAttribute('value') == "on"){
//            data["status"] = 1;
//        }
//        $http.post(SERVER+"/api/logs/"+username+"/"+symptom_id+"/update", data).then(
//            function(response){
//                console.log(response);
//            }
//        );
    }
});


epilog.controller('LogCreateCtrl', function ($scope, $stateParams, SERVER, $http) {
//    $scope.symptom = "";
//    $scope.affected_area = "";
//    $scope.person = "";
//    $scope.description = "";
//    $scope.fileName = "";
    $scope.logData = {};
    var username = $scope.getUserID();
    // var $file = document.getElementById('file');
    // $scope.openFileDialog=function() {
    //     console.log('fire! $scope.openFileDialog()');
    //     ionic.trigger('click', { target: $file });
    // };

  
    // angular.element($file).on('change',function(event) {
    //     console.log('fire! angular#element change event');
       
    //     var file = event.target.files[0];
    //     $scope.fileName=file.name;
    //     $scope.$apply();
    // });
    
    $scope.reset = function(){
        $scope.logData = {};
    }
    
    $scope.submit = function(){
    
        var data = {
            "username": username,
            "symptom": $scope.logData.symptom,
            "affected_area": $scope.logData.affected_area,
            "description": $scope.logData.description,
            "person": $scope.logData.person
        };
        $http.post(SERVER+"/api/symptoms/add", data).then(
            function(response){
                console.log(response);
            },
            function(response){
                console.log("Error occured")
            }
        );
    };    
});




epilog.controller('MapCtrl', function ($scope, GEOMap, $ionicLoading, $compile, $rootScope) {


    var mapElement = document.getElementById('map');
    try {
        GEOMap.showGeoMap(new Array(17.97075, -76.763163), "Click Me", 'Uluru (Ayers Rock)');
        google.maps.event.addDomListener(window, 'load', GEOMap.geoMap(mapElement, $scope));
    } catch (e) {}


    $scope.centerOnMe = function () {
        if (!$scope.map) {
            return;
        }

        $scope.loading = $ionicLoading.show({

            content: '<i class=" ion-loading-c"></i> ' + 'Getting current location...',

            // The animation to use
            animation: 'fade-in',
            // Will a dark overlay or backdrop cover the entire view
            showBackdrop: true,
            // The maximum width of the loading indicator
            // Text will be wrapped if longer than maxWidth
            maxWidth: 200,
            // The delay in showing the indicator
            showDelay: 500

        });

        navigator.geolocation.getCurrentPosition(function (pos) {
            myLatlng=new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);
            $scope.map.setCenter(myLatlng);
            GEOMap.addMarker(myLatlng, $scope.map);
            $ionicLoading.hide();
        }, function (error) {
            alert('Unable to get location: ' + error.message);
        });
    };

    $scope.clickTest = function () {
        alert('Example of infowindow with ng-click')
    };

});