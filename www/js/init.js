// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
//var epilog=angular.module('epilog', ['ionic', 'starter.controllers','starter.services']);
var epilog = angular.module('epilog', ['ionic', 'firebase']);


epilog.run(function($ionicPlatform, $rootScope) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });

    $rootScope.getUserID = function(){
        console.log("get user data.")
        var userdata = localStorage.getItem("userdata");
        if(userdata == undefined){
            return "";
        }
        var juser = JSON.parse(userdata);
        console.log(juser);
        return juser["username"];
    }
    
    $rootScope.pageID = "default";
    $rootScope.pageTitle = "Epilog Center";
  
});

epilog.constant('FURL', 'https://asfirebase.firebaseio.com');
epilog.constant('SERVER', 'https://epilog-server-oshanyb4.c9users.io');