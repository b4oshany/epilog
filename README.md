# Epi.Log

logs for treating and preventing epidemics
-----------

## Contents
- What is Epi.log?
- Goals
- Specifications
- Milestones(Todo)
  1. Logs
  2. Notifications
  3. Tutorial of treatment and prevention
  4. Risk Map
  5. Dashboard/API

### What is **Epi.Log**? 

Project epi.log is an effort to help mitigate the outbreak of contagious illnesses within the Caribbean Region.  The end goal is a multi-platform application, built on the ionic framework, which offers several features to help individuals reduce their risk of illnesses and reduce sources of illness facilitation in their surroundings. The name *epi.log* is a clever play on words, stemming from the words **“epidemic”** and **“log”**. The words can exhibit the process taken to reduce the spreading of illnesses; *epidemic, log, epilog*. The application will alert of illnesses (growing possibilities of an epidemic) nearby that users may be at risk of, and also allow users to log/report the number of risks within their household and/or workplace. There will then be an epilog provided with tutorials for methods of treatment and prevention for those illnesses the user may be aware and at risk of. A map will be available within the application to highlight the infection density of areas. And layers made available to see areas vulnerable to infection.
The purpose of epi.log is not just for the safety and well being of the users, but also to provide governments, ministries, medical institutions and interested 3rd parties, with real time updates on population health status with regards to infectious diseases. This will be done through an API. This should eventually comply with Open Data policies to be put in place by GoJ. 

Goals
----
- **Infectious disease notification: **Notify users when they may be at risk of virus, be it vector-based, or other contagious illnesses. This will be based off a radius of frequently visited places, along with an area of frequent/recent paths travelled and the nature of the virus.

- **Walk-through of treatments and prevention: ** A tutorial type slide to educate users on how to treat and prevent catching these illnesses.

- **Real-time map of infection density: ** pulls from the platforms data to  display the density of viral activity in the region. Filters will be available to facilitate multiple views. Filters will include time frame, virus (type), density.

- **Dashboard and API for governments, ministries and other 3rd parties to plug into: **There will be a method provided for authorized entities to plug into the  platform’s data and capabilities.

Specifications
--------
The application will be developed on the ionic framework to facilitate multi-platform availability since the Caribbean mobile market is diverse. The application will not be resource intensive thus able to run on low end (budget) devices. This is to facilitate even the rural communities to participate on health risk mitigation.  Anonymity of the users is something of importance, even if accounts are to be supported.

Milestones(Todo)
---------
1. **Log**(in progress): The log will allow for a user to enter one or more symptoms they are experiencing and an aggregate analysis of the symptoms will give probable infections they may be undergoing. The process is explained below of how a symptom would be entered.

   - Symptom: (autocomplete of suggestions),

   - Where: (autocomplete of suggestions)
   - Take photo (if it is a physical symptom)

  The user will be able to then select from the suggestions and an option will be provided (doctor’s diagnosis confirmed), which will tell whether the log entry is in tandem with a doctor’s diagnosis, if the user so chooses to confirm with a doctor.
This log will also save the user’s last 3 days of location data (taken from Google) to help provide useful data for analysis.
2. **Notification**: Users of the application will receive notification of viruses  that are becoming prevalent in their environment. This will occur for their home environments, work environments, and other frequently visited environment. This will alert them based off of a radius, which may vary depending on the virus.

3. **Tutorial of treatment and prevention**(in progress): Using a material-intro like experience, users will be able to access tutorials which will explain steps to take for treatment and prevention of illnesses.
4. **Risk Map**(in progress): A map, using OpenStreetMaps and compatible tools, will be provided to support real time view of illnesses, this may be filtered by illness, recovery, and a period of time. Provide layers for 3rd party filters

5. **Dashboard/API**(in progress): An API will be provided for stakeholders, authorised upon request, to access the data collected by the application.

-Christopher Lee Murray (CEO/Team Leader/Software Architect)

## Status
APLHA
## Setup
### Support
- TBA

### Dependencies
- install [ionic framework](http://ionicframework.com/docs/guide/installation.html)
- install node.js (for Linux: <code>sudo apt-get install nodejs</code>)
- install git(for Linux: <code>sudo apt-get install git</code>)
- clone (copy) the git repository by running <code>git clone https://gitlab.com/b4oshany/epilog/</code>
- run <code>npm install -g cordova ionic</code>
- run <code>sudo npm install -g ionic</code>
- <code>cd</code> into app directory then run <code>ionic platform add android</code>
### Build
- run <code>ionic build android</code>
- to emulate run run <code>ionic emulate android</code>


## Screenshots
![login](https://gitlab.com/b4oshany/epilog/raw/master/mockups/screens/login.png)

![signup](https://gitlab.com/b4oshany/epilog/raw/master/mockups/screens/sign_up.png)

![login](https://gitlab.com/b4oshany/epilog/raw/master/mockups/screens/login.png)

![health](https://gitlab.com/b4oshany/epilog/raw/master/mockups/screens/health.png)

![tutorial_example_1](https://gitlab.com/b4oshany/epilog/raw/master/mockups/screens/tutorial_example_1.png)

![tutorial_example_2](https://gitlab.com/b4oshany/epilog/raw/master/mockups/screens/tutorial_example_2.png)

![tutorial_example_3](https://gitlab.com/b4oshany/epilog/raw/master/mockups/screens/tutorial_example_3.png)

![sidebar](https://gitlab.com/b4oshany/epilog/raw/master/mockups/screens/sidebar.png)

![area](https://gitlab.com/b4oshany/epilog/raw/master/mockups/screens/area.png)

![alerts](https://gitlab.com/b4oshany/epilog/raw/master/mockups/screens/alerts.png)

![permissions](https://gitlab.com/b4oshany/epilog/raw/master/mockups/screens/person.png)

![log](https://gitlab.com/b4oshany/epilog/raw/master/mockups/screens/log.png)

![symptoms](https://gitlab.com/b4oshany/epilog/raw/master/mockups/screens/symptoms.png)

![symptoms_risk](https://gitlab.com/b4oshany/epilog/raw/master/mockups/screens/symptoms_risk.png)

![dashboard](https://gitlab.com/b4oshany/epilog/raw/master/mockups/screens/dashboard.png)

![dashboard_my_health](https://gitlab.com/b4oshany/epilog/raw/master/mockups/screens/dashboard_my_health.png)

![dashboard_environment](https://gitlab.com/b4oshany/epilog/raw/master/mockups/screens/dashboard_environment.png)

![dashboard_household](https://gitlab.com/b4oshany/epilog/raw/master/mockups/screens/dashboard_household.png)

![dashboard_householddisclaimer](https://gitlab.com/b4oshany/epilog/raw/master/mockups/screens/dashboard_householddisclaimer.png)

![addperson](https://gitlab.com/b4oshany/epilog/raw/master/mockups/screens/addperson.png)

## Character Sets
Designed by Brittney. To be used for tutorials for treatments and prevention. These are found in the "pngs" folder.


## Credits

- Brittney Samuels (Designer)
- Christopher Lee Murray (UI/UX/Architect)
- Oshane Bailey (Developer/Engineer)
- Denar Brown (Developer)
